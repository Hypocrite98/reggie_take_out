package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itheima.reggie.dto.*;
import com.itheima.reggie.entity.*;
import com.itheima.reggie.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/statics")
public class SaticsController {

    @Autowired
    private StaticsService staticsService;

    @Autowired
    private OrderService orderService;
    @Autowired
    private OrderDetailService orderDetailService;

    @Autowired
    private DishService dishService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private SetmealService setmealService;

    /**
     * 菜系营收占比分析(菜品名称和菜品卖出去的总价格)
     *
     * @return
     */
    @GetMapping("/dishCash")
    public DishCategoryCashDto selectDishCategoryCash() {
        DishCategoryCashDto dishCategoryCashDto = new DishCategoryCashDto();
        List<Statics> dishCategoryCash = dishCategoryCashDto.getDishCategoryCash();

        // 先获取到当前数据库中所有的菜系
        // select name from category where type =1;
        QueryWrapper<Category> qw = new QueryWrapper<>();
        qw.eq("type", 1);
        qw.select("name");
        List<Category> arrayList = categoryService.list(qw);

        // 获取到所有的订单明细
        List<OrderDetail> list = orderDetailService.list();
        // 遍历每一个category_name 和 每一个订单
        // 统计每一个category_name对应的Value
        for (int i = 0; i < arrayList.size(); i++) {
            BigDecimal dishValue = new BigDecimal(0);
            for (OrderDetail orderDetail : list) {
                Long dishId = orderDetail.getDishId();
                // 根据dishId判断是套餐还是菜系
                if (dishId == null) {// 套餐统计
                    continue;
                }
                // 菜系统计
                Dish dish = dishService.getById(dishId);
                Long categoryId = dish.getCategoryId();
                Category category = categoryService.getById(categoryId);
                String categoryName = category.getName();
                if (arrayList.get(i).getName().equals(categoryName)) {
                    BigDecimal amount = orderDetail.getAmount();
                    Integer numberInteger = orderDetail.getNumber();
                    int numberInt = numberInteger.intValue();
                    BigDecimal number = new BigDecimal(numberInt);
                    // value = value + number * amount
                    dishValue = dishValue.add(number.multiply(amount));
                }
            }
            // 菜系数据封装
            Statics Dishstatics = new Statics();
            Dishstatics.setName(arrayList.get(i).getName());
            Dishstatics.setValue(dishValue);
            dishCategoryCash.add(Dishstatics);
        }
        return dishCategoryCashDto;
    }

    /**
     * 套餐营收占比分析
     *
     * @return
     */
    @GetMapping("/setmealCash")
    public SetmealCashDto selectSetmealCash() {
        SetmealCashDto setmealCashDto = new SetmealCashDto();
        List<Statics> setmealCash = setmealCashDto.getSetmealCash();

        // 先获取到当前数据库中所有的套餐
        // select name from category where type =2;
        QueryWrapper<Category> qw = new QueryWrapper<>();
        qw.eq("type", 2);
        qw.select("name");
        List<Category> arrayList = categoryService.list(qw);

        // 获取到所有的订单明细
        List<OrderDetail> list = orderDetailService.list();
        // 遍历每一个category_name 和 每一个订单
        // 统计每一个category_name对应的Value
        for (int i = 0; i < arrayList.size(); i++) {
            BigDecimal setmealValue = new BigDecimal(0);
            for (OrderDetail orderDetail : list) {
                Long dishId = orderDetail.getDishId();
                // 根据dishId判断是套餐还是菜系
                if (dishId == null) {// 套餐统计
                    Long setmealId = orderDetail.getSetmealId();
                    Setmeal setmeal = setmealService.getById(setmealId);
                    Long categoryId = setmeal.getCategoryId();
                    Category category = categoryService.getById(categoryId);
                    String categoryName = category.getName();
                    if (arrayList.get(i).getName().equals(categoryName)) {
                        BigDecimal amount = orderDetail.getAmount();
                        Integer numberInteger = orderDetail.getNumber();
                        int numberInt = numberInteger.intValue();
                        BigDecimal number = new BigDecimal(numberInt);
                        // value = value + number * amount
                        setmealValue = setmealValue.add(number.multiply(amount));
                    }
                }
            }
            // 套餐数据封装
            Statics setmealstatics = new Statics();
            setmealstatics.setName(arrayList.get(i).getName());
            setmealstatics.setValue(setmealValue);
            setmealCash.add(setmealstatics);
        }
        return setmealCashDto;
    }

    /**
     * 菜系销量占比分析
     *
     * @return
     */
    @GetMapping("/dishNum")
    public DishNumDto selectDishNum() {
        DishNumDto dishNumDto = new DishNumDto();
        List<Statics> dishNum = dishNumDto.getDishNum();

        // 先获取到当前数据库中所有的菜系
        // select name from category where type =1;
        QueryWrapper<Category> qw = new QueryWrapper<>();
        qw.eq("type", 1);
        qw.select("name");
        List<Category> arrayList = categoryService.list(qw);

        // 获取到所有的订单明细
        List<OrderDetail> list = orderDetailService.list();
        // 遍历每一个category_name 和 每一个订单
        // 统计每一个category_name对应的Value
        for (int i = 0; i < arrayList.size(); i++) {
            BigDecimal dishValue = new BigDecimal(0);
            for (OrderDetail orderDetail : list) {
                Long dishId = orderDetail.getDishId();
                // 根据dishId判断是套餐还是菜系
                if (dishId == null) {
                    continue;
                }
                Dish dish = dishService.getById(dishId);
                Long categoryId = dish.getCategoryId();
                Category category = categoryService.getById(categoryId);
                String categoryName = category.getName();
                if (arrayList.get(i).getName().equals(categoryName)) {
                    Integer numberInteger = orderDetail.getNumber();
                    int numberInt = numberInteger.intValue();
                    BigDecimal number = new BigDecimal(numberInt);
                    dishValue = dishValue.add(number);
                }
            }
            // 菜系数据封装
            Statics Dishstatics = new Statics();
            Dishstatics.setName(arrayList.get(i).getName());
            Dishstatics.setValue(dishValue);
            dishNum.add(Dishstatics);
        }
        return dishNumDto;
    }

    /**
     * 套餐销量占比分析
     *
     * @return
     */
    @GetMapping("/setmealNum")
    public SetmealNumDto selectSetmealNum() {
        SetmealNumDto setmealNumDto = new SetmealNumDto();
        List<Statics> setmealNum = setmealNumDto.getSetmealNum();

        // 先获取到当前数据库中所有的套餐
        // select name from category where type =2;
        QueryWrapper<Category> qw = new QueryWrapper<>();
        qw.eq("type", 2);
        qw.select("name");
        List<Category> arrayList = categoryService.list(qw);

        // 获取到所有的订单明细
        List<OrderDetail> list = orderDetailService.list();

        // 遍历每一个category_name 和 每一个订单
        // 统计每一个category_name对应的Value
        for (int i = 0; i < arrayList.size(); i++) {
            BigDecimal setmealValue = new BigDecimal(0);
            for (OrderDetail orderDetail : list) {
                Long dishId = orderDetail.getDishId();
                // 根据dishId判断是套餐还是菜系
                if (dishId != null) {
                    continue;
                }
                Long setmealId = orderDetail.getSetmealId();
                Setmeal setmeal = setmealService.getById(setmealId);
                Long categoryId = setmeal.getCategoryId();
                Category category = categoryService.getById(categoryId);
                String categoryName = category.getName();
                if (arrayList.get(i).getName().equals(categoryName)) {
                    Integer numberInteger = orderDetail.getNumber();
                    int numberInt = numberInteger.intValue();
                    BigDecimal number = new BigDecimal(numberInt);
                    setmealValue = setmealValue.add(number);
                }
            }
            // 菜系数据封装
            Statics setmealStatics = new Statics();
            setmealStatics.setName(arrayList.get(i).getName());
            setmealStatics.setValue(setmealValue);
            setmealNum.add(setmealStatics);
        }
        return setmealNumDto;
    }

    /**
     * 月营业额趋势图(近1年)
     *
     * @return
     */
    @GetMapping("/cashPerMonth")
    public CashPerMonthDto selectCashPerMonthDto() {
        CashPerMonthDto cashPerMonthDto = new CashPerMonthDto();
        List<String> monthList = cashPerMonthDto.getMonthList();
        List<Integer> dataList = cashPerMonthDto.getDataList();

        // 获取最新的那条订单的结账时间
        // 然后向前推1年
        // select checkout_time  from orders order by checkout_time desc limit 1;
        QueryWrapper<Orders> qw = new QueryWrapper<>();
        qw.orderByDesc("checkout_time");
        qw.select("checkout_time");
        List<Orders> ordersList1 = orderService.list(qw);
        LocalDateTime time = ordersList1.get(0).getCheckoutTime();
        int year = time.getYear();
        int month = time.getMonth().getValue();// 2022-09-26T23:37:34

        for (int i = 0; i < 12; i++) {
            if (0 < month && month < 10) {
                monthList.add(year + "-0" + month);
            } else {
                monthList.add(year + "-" + month);
            }
            month--;
            if (month == 0) {
                month = 12;
                year--;
            }
        }
        List<Orders> ordersList = orderService.list();
        for (String s : monthList) {// 2022-09,2022-11...
            // LocalDateTime localDateTime = LocalDateTime.parse(s, DateTimeFormatter.ofPattern("yyyy-MM"));
            int sum = 0;
            for (Orders order : ordersList) {
                LocalDateTime checkoutTime = order.getCheckoutTime();
                int timeInt = checkoutTime.getMonth().getValue();
                String timeStr = Integer.valueOf(timeInt).toString();// 9,11
                // 处理时间
                String s1 = s.substring(5);// 09,11
                String str = s1;
                if (s1.charAt(0) == '0') {
                    str = s1.substring(1);// 9,11
                }

                if (str.equals(timeStr)) {
                    BigDecimal amountBD = order.getAmount();
                    int amount = amountBD.intValue();
                    sum += amount;
                }
            }
            dataList.add(sum);
        }
        return cashPerMonthDto;
    }
}
