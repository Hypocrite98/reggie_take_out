package com.itheima.reggie.controller;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.ObjectMetadata;
import com.aliyun.oss.model.PutObjectRequest;
import com.itheima.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.lang.reflect.Field;
import java.util.UUID;

/**
 * 文件上传和下载
 */
@RestController
@RequestMapping("/common")
@Slf4j
public class  CommonController {

    @Value("${reggie.path}")
    private String basePath;// D:\img\

    /**
     * 文件上传
     * 上传到本地服务器上
     * @param file
     * @return
     */
    /*@PostMapping("/upload")
    public R<String> upload(MultipartFile file) {
        //file是一个临时文件，需要转存到指定位置，否则本次请求完成后临时文件会删除
        log.info(file.toString());

        //1.文件名称的替换 abc.jpg--->dfsdfdfd.jpg
        //原始文件名
        String originalFilename = file.getOriginalFilename();//abc.jpg
        //获取文件后缀 .jpg
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        //使用UUID重新生成文件名，防止文件名称重复造成文件覆盖
        String fileName = UUID.randomUUID() + suffix;//dfsdfdfd.jpg

        //2.文件的上传实现
        //创建一个目录对象
        File dir = new File(basePath);//D:\img\
        //判断当前目录是否存在
        if (!dir.exists()) {
            //目录不存在，需要创建
            dir.mkdirs();
        }
        try {
            //将临时文件转存到指定位置
            file.transferTo(new File(basePath + fileName));//D:\img\dfsdfdfd.jpg
        } catch (IOException e) {
            e.printStackTrace();
        }
        return R.success(fileName);
    }*/

    /**
     * 文件上传改造
     * 阿里云OSS
     *
     * @param file
     * @return
     */
    @PostMapping("/upload")
    public R upload(MultipartFile file) throws IOException {
        // file是一个临时文件，需要转存到指定位置，否则本次请求完成后临时文件会删除
        log.info(file.toString());

        // 1.文件名称的替换 abc.jpg --> dfsdfdfd.jpg
        // 原始文件名
        String originalFilename = file.getOriginalFilename();// abc.jpg
        // 获取文件后缀 .jpg
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        // 使用UUID重新生成文件名，防止文件名称重复造成文件覆盖
        String fileName = UUID.randomUUID() + suffix;// dfsdfdfd.jpg

        // 2.文件的上传实现
        // Endpoint以华东1（杭州）为例，其它Region请按实际情况填写。
        String endpoint = "https://oss-cn-hangzhou.aliyuncs.com";
        // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
        String accessKeyId = "LTAI5tDji4Zwy3rtemwedUbF";
        String accessKeySecret = "LuI2GBwakHLW9rfzeWLbscKVOykiDy";
        // 填写Bucket名称，例如examplebucket。
        String bucketName = "bucket1-reggie";
        // 填写Object完整路径，完整路径中不能包含Bucket名称，例如exampledir/exampleobject.txt。
        String objectName = "reggie/" + fileName;// reggie/dfsdfdfd.jpg
        // 填写本地文件的完整路径，例如D:\\localpath\\examplefile.txt。
        // 如果未指定本地路径，则默认从示例程序所属项目对应本地路径中上传文件。
        // String filePath = basePath + 获取file的路径;// D:\img\dfsdfdfd.jpg
        InputStream inputStream = file.getInputStream();

        // 创建OSSClient实例。
        OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

        try {
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("image/jpg");
            // 创建PutObjectRequest对象。
            // PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, new File(filePath),metadata);
            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, objectName, inputStream,metadata);
            // 如果需要上传时设置存储类型和访问权限，请参考以下示例代码。
            // ObjectMetadata metadata = new ObjectMetadata();
            // metadata.setHeader(OSSHeaders.OSS_STORAGE_CLASS, StorageClass.Standard.toString());
            // metadata.setObjectAcl(CannedAccessControlList.Private);
            // putObjectRequest.setMetadata(metadata);

            // 上传文件。
            ossClient.putObject(putObjectRequest);
            endpoint = endpoint.replaceAll("https://", "");
            String url = "https://" + bucketName + "." + endpoint + "/" + objectName;
            return R.success(url);
        } catch (OSSException oe) {
            System.out.println("Caught an OSSException, which means your request made it to OSS, "
                    + "but was rejected with an error response for some reason.");
            System.out.println("Error Message:" + oe.getErrorMessage());
            System.out.println("Error Code:" + oe.getErrorCode());
            System.out.println("Request ID:" + oe.getRequestId());
            System.out.println("Host ID:" + oe.getHostId());
            return null;
        } catch (ClientException ce) {
            System.out.println("Caught an ClientException, which means the client encountered "
                    + "a serious internal problem while trying to communicate with OSS, "
                    + "such as not being able to access the network.");
            System.out.println("Error Message:" + ce.getMessage());
            return null;
        } finally {
            if (ossClient != null) {
                ossClient.shutdown();
            }
        }
    }

    /**
     * 文件下载
     *
     * @param name
     * @param response
     */
    @GetMapping("/download")
    public void download(String name, HttpServletResponse response) {
        /*try {
            //1.将服务器的文件读入到内存中(此时服务器就是本地硬盘)
            //输入流，通过输入流读取文件内容
            FileInputStream fileInputStream = new FileInputStream((basePath + name));

            //2.将内存中的文件写出,由浏览器进行响应展示
            //输出流，通过输出流将文件写回浏览器
            ServletOutputStream outputStream = response.getOutputStream();
            response.setContentType("image/jpeg");
            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = fileInputStream.read(bytes)) != -1){
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }
            //关闭资源
            outputStream.close();
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        /*try {
            // response.setHeader("Content-Disposition","attachment; filename="+name);// 强制要求浏览器下载。
            // response.setContentType("image/jpeg");// 告知浏览器响应的内容类型
            FileInputStream fileInputStream = new FileInputStream(new File(basePath, name));
            ServletOutputStream outputStream = response.getOutputStream();
            IOUtils.copy(fileInputStream, outputStream);
            fileInputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/


    }
}
