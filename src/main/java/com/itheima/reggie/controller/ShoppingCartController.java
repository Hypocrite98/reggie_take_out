package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 购物车
 */
@Slf4j
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 添加购物车
     * 关键在于要判断这道菜是否已存在购物车中
     * 如果已存在,用户选择加入购物车,则数量+1,sql语句为update
     * 如果不在,用户选择加入购物车,则数量默认为1,sql语句为insert
     * @param shoppingCart
     * @return
     */
    @PostMapping("/add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart){
        log.info("购物车数据:{}",shoppingCart);
        //设置用户id，指定当前是哪个用户的购物车数据
        Long currentId = BaseContext.getCurrentId();
        shoppingCart.setUserId(currentId);
        Long dishId = shoppingCart.getDishId();
        LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
        qw.eq(ShoppingCart::getUserId,currentId);
        if(dishId != null){
            //添加到购物车的是菜品
            qw.eq(ShoppingCart::getDishId,dishId);
        }else{
            //添加到购物车的是套餐
            qw.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());
        }
        //查询当前菜品或者套餐是否在购物车中
        //SQL:select * from shopping_cart where user_id = ? and dish_id/setmeal_id = ?
        ShoppingCart cartServiceOne = shoppingCartService.getOne(qw);
        if(cartServiceOne != null){
            //如果已经存在，就在原来数量基础上加一
            Integer number = cartServiceOne.getNumber();
            cartServiceOne.setNumber(number + 1);
            shoppingCartService.updateById(cartServiceOne);
        }else{
            //如果不存在，则添加到购物车，数量默认就是一
            shoppingCart.setNumber(1);
            shoppingCart.setCreateTime(LocalDateTime.now());
            shoppingCartService.save(shoppingCart);
            cartServiceOne = shoppingCart;
        }
        return R.success(cartServiceOne);
    }

    /**
     * 查看购物车
     * @return
     */
    @GetMapping("/list")
    public R<List<ShoppingCart>> list(){
        log.info("查看购物车...");
        LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
        qw.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());
        qw.orderByAsc(ShoppingCart::getCreateTime);
        List<ShoppingCart> list = shoppingCartService.list(qw);
        return R.success(list);
    }

    /**
     * 取消购物车
     */
    @PostMapping("/sub")
    public R subShoppingCart(@RequestBody ShoppingCart shoppingCart){
        //菜品或者套餐id 查询出当前ShoppingCart数据
        // 如何是菜品：select * from shopping_cart where dish_id = ? and user_id = ?
        // 如果是套餐：select * from shopping_cart where setmeal_id = ? and user_id = ?
        LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
        Long userId = BaseContext.getCurrentId();
        qw.eq(ShoppingCart::getUserId,userId);//where user_id =?
        Long dishId = shoppingCart.getDishId();
        if(dishId!=null){//是菜品
            qw.eq(ShoppingCart::getDishId,dishId);// dish_id = ?
        }else {// 是套餐
            qw.eq(ShoppingCart::getSetmealId,shoppingCart.getSetmealId());// setmeal_id = ?
        }
        ShoppingCart one = shoppingCartService.getOne(qw);
        // number -1 然后操作：
        Integer number = one.getNumber();
        number--;
        one.setNumber(number);
        if(number>0){
            // >0  update更新 update set number=? where id = ?
            shoppingCartService.updateById(one);
        }else {
            // <= 0  如何操作？ delete from shopping_cart where dish_id=? and user_id = ?
            shoppingCartService.remove(qw);
        }
        return R.success(one);
    }

    /**
     * 清空购物车
     * @return
     */
    @DeleteMapping("/clean")
    public R<String> clean(){
        //SQL:delete from shopping_cart where user_id = ?
        LambdaQueryWrapper<ShoppingCart> qw = new LambdaQueryWrapper<>();
        qw.eq(ShoppingCart::getUserId,BaseContext.getCurrentId());
        shoppingCartService.remove(qw);
        return R.success("清空购物车成功");
    }
 }   