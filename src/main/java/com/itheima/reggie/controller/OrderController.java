package com.itheima.reggie.controller;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.support.ExcelTypeEnum;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.OrderDto;
import com.itheima.reggie.entity.OrderDetail;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.service.OrderDetailService;
import com.itheima.reggie.service.OrderService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * 订单
 */

@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private OrderService orderService;

    @Autowired
    private OrderDetailService orderDetailService;

    /**
     * 用户下单
     *
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    public R<String> submit(@RequestBody Orders orders) {
        log.info("订单数据：{}", orders);
        orderService.submit(orders);
        return R.success("下单成功");
    }

    /**
     * 分页查询订单(手机端)
     * 前端需要每一条记录的orderDetails,所以需要使用OrderDto封装数据
     */
    @GetMapping("/userPage")
    public R<Page<OrderDto>> selectByPage(int page, int pageSize) {
        Page<Orders> orderPage = new Page<>(page, pageSize);
        Page<OrderDto> orderDtoPage = new Page<>();
        LambdaQueryWrapper<Orders> qw = new LambdaQueryWrapper<>();
        Long userId = BaseContext.getCurrentId();
        qw.eq(Orders::getUserId, userId);
        qw.orderByAsc(Orders::getOrderTime);
        orderService.page(orderPage, qw);
        // 将orderPage中除了属性"records"外的内容拷贝到orderDtoPage
        BeanUtils.copyProperties(orderDtoPage, orderDtoPage, "records");
        // 再将orderPage中的records改造赋值为orderDtoPage
        List<Orders> records = orderPage.getRecords();
        List<OrderDto> list = records.stream().map(new Function<Orders, OrderDto>() {
            @Override
            public OrderDto apply(Orders orders) {
                OrderDto orderDto = new OrderDto();
                // 先拷贝原来的records
                BeanUtils.copyProperties(orders, orderDto);
                // 再给OrderDto的属性orderDetails赋值
                Long orderId = orders.getId();
                LambdaQueryWrapper<OrderDetail> qw = new LambdaQueryWrapper<>();
                qw.eq(OrderDetail::getOrderId, orderId);
                List<OrderDetail> orderDetails = orderDetailService.list(qw);
                orderDto.setOrderDetails(orderDetails);
                return orderDto;
            }
        }).collect(Collectors.toList());
        orderDtoPage.setRecords(list);
        return R.success(orderDtoPage);
    }

    /**
     * 分页查询订单(网页端)
     * 前端需要每一条记录的orderDetails,所以需要使用OrderDto封装数据
     */
    @GetMapping("/page")
    public R<Page<OrderDto>> selectByPage(int page, int pageSize, Long number,
                                          @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime beginTime,
                                          @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss") LocalDateTime endTime) {
        Page<Orders> orderPage = new Page<>(page, pageSize);
        Page<OrderDto> orderDtoPage = new Page<>();
        LambdaQueryWrapper<Orders> qw = new LambdaQueryWrapper<>();
        qw.eq(number != null, Orders::getNumber, number);
        qw.between(beginTime != null && endTime != null, Orders::getOrderTime, beginTime, endTime);
        qw.orderByDesc(Orders::getOrderTime);
        orderService.page(orderPage, qw);
        // 将orderPage中除了属性"records"外的内容拷贝到orderDtoPage
        BeanUtils.copyProperties(orderPage, orderDtoPage, "records");
        // 再将orderPage中的records改造赋值为orderDtoPage
        List<Orders> records = orderPage.getRecords();
        List<OrderDto> list = records.stream().map(new Function<Orders, OrderDto>() {
            @Override
            public OrderDto apply(Orders orders) {
                OrderDto orderDto = new OrderDto();
                // 先拷贝原来的records
                BeanUtils.copyProperties(orders, orderDto);
                // 再给OrderDto的属性orderDetails赋值
                Long orderId = orders.getId();
                LambdaQueryWrapper<OrderDetail> qw = new LambdaQueryWrapper<>();
                qw.eq(OrderDetail::getOrderId, orderId);
                List<OrderDetail> orderDetails = orderDetailService.list(qw);
                orderDto.setOrderDetails(orderDetails);
                return orderDto;
            }
        }).collect(Collectors.toList());
        orderDtoPage.setRecords(list);
        return R.success(orderDtoPage);
    }

    /**
     * 导出Excel
     */
    // @GetMapping("/export")
    /*public R exportOrderExcel() {
        *//*1、查询所有的订单数据 List<>
        2、写入excel 【apache poi | 阿里的easy excel】
        3、将excel通过response响应流写入到浏览器
        4、下载的文件名需要写死： 订单明细.xlsx 响应头：Content-Disposition *//*

    }*/

    @SneakyThrows(IOException.class)
    @GetMapping("/export")
    public void exportMemberList(HttpServletResponse response) {
        setExcelRespProp(response, "订单列表");
        List<Orders> list = orderService.list();
        EasyExcel.write(response.getOutputStream())
                .head(Orders.class)
                .excelType(ExcelTypeEnum.XLSX)
                .sheet("订单列表")
                .doWrite(list);
    }

    /**
     * 设置excel下载响应头属性
     */
    private void setExcelRespProp(HttpServletResponse response, String rawFileName) throws UnsupportedEncodingException {
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setCharacterEncoding("utf-8");
        String fileName = URLEncoder.encode(rawFileName, "UTF-8").replaceAll("\\+", "%20");
        response.setHeader("Content-disposition", "attachment;filename*=utf-8''" + fileName + ".xlsx");
    }

}