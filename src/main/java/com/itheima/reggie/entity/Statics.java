package com.itheima.reggie.entity;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Slf4j
@Data
public class Statics {

    private BigDecimal value;

    private String name;
}
