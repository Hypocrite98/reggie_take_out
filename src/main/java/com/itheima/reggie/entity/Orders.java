package com.itheima.reggie.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.format.DateTimeFormat;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 订单
 */
@Data
public class Orders implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;

    // 订单号
   @ExcelProperty("订单号")
    private String number;

    // 订单状态 1待付款，2待派送，3已派送，4已完成，5已取消
    @ExcelProperty("订单号")
    private Integer status;

    // 下单用户id
    @ExcelProperty("下单用户id")
    private Long userId;

    // 地址id
    private Long addressBookId;

    // 下单时间
    @DateTimeFormat("yyyy-MM-dd")
    @ExcelProperty("下单时间")
    private LocalDateTime orderTime;

    // 结账时间
    private LocalDateTime checkoutTime;

    // 支付方式 1微信，2支付宝
    private Integer payMethod;

    // 实收金额
    private BigDecimal amount;

    // 备注
    private String remark;

    // 用户名
    private String userName;

    // 手机号
    @ExcelProperty("手机号")
    private String phone;

    // 地址
    @ExcelProperty("地址")
    private String address;

    // 收货人
    private String consignee;
}