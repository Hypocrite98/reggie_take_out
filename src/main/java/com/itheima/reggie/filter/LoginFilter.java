package com.itheima.reggie.filter;

import com.alibaba.fastjson.JSON;
import com.itheima.reggie.common.BaseContext;
import com.itheima.reggie.common.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.AntPathMatcher;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Slf4j
@WebFilter("/*")
public class LoginFilter implements Filter {
    public void init(FilterConfig config) throws ServletException {
    }

    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws ServletException, IOException {
        //ant风格路径匹配工具类
        AntPathMatcher PATH_MATCHER = new AntPathMatcher();
        String[] urls = new String[]{
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/common/**",
                "/user/login",
                "/user/sendMsg",
                "/doc.html",
                "/webjars/**",
                "/swagger-resources",
                "/v2/api-docs"
        };
        HttpServletRequest req = (HttpServletRequest) request;
        String requestURI = req.getRequestURI();
        for (String url : urls) {
            boolean matchResult = PATH_MATCHER.match(url, requestURI);
            if (matchResult) {
                //请求放行
                chain.doFilter(request, response);
                return;//结束当前方法
            }
        }

        //通过session判断管理端用户是否已登录
        Object id = req.getSession().getAttribute("employee");
        if (id != null) {
            //将session中的id存入ThreadLocal中
            Long empId = (Long) req.getSession().getAttribute("employee");
            BaseContext.setCurrentId(empId);
            chain.doFilter(request, response);
            return;//结束当前方法
        }

        //通过session判断移动端用户是否已登录
        if (req.getSession().getAttribute("user") != null) {
            log.info("用户已登录，用户id为：{}", req.getSession().getAttribute("user"));
            Long userId = (Long) req.getSession().getAttribute("user");
            BaseContext.setCurrentId(userId);
            chain.doFilter(request, response);
            return;//结束当前方法
        }

        HttpServletResponse res = (HttpServletResponse) response;
        R<Object> notlogin = R.error("NOTLOGIN");
        String jsonString = JSON.toJSONString(notlogin);
        res.getWriter().write(jsonString);
    }
}
