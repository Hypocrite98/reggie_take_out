package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.entity.Statics;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface StaticsMapper extends BaseMapper<Statics> {
}