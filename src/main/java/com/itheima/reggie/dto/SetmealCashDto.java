package com.itheima.reggie.dto;

import com.itheima.reggie.entity.Statics;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SetmealCashDto {

    private List<Statics> setmealCash = new ArrayList();

}
