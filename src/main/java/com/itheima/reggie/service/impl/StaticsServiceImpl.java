package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.entity.Statics;
import com.itheima.reggie.mapper.StaticsMapper;
import com.itheima.reggie.service.StaticsService;
import org.springframework.stereotype.Service;

@Service
public class StaticsServiceImpl extends ServiceImpl<StaticsMapper, Statics> implements StaticsService {
}
