package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Statics;

public interface StaticsService extends IService<Statics> {
}
