package com.itheima.sort;

import java.util.Arrays;

public class bubble_v2 {
    public static void main(String[] args) {
        //bubble(new int[]{5, 4, 1, 9, 7});
        bubble_v2(new int[]{5, 4, 1, 9, 7});
    }

    public static void bubble(int[] a) {
        //每轮冒泡都会将本轮最大的数排到末尾
        //每经过一轮冒泡，内层循环就可以减少一次
        for (int j = 0; j < a.length - 1; j++) {
            // 一轮冒泡
            boolean swapped = false; // 是否发生了交换
            for (int i = 0; i < a.length - 1 - j; i++) {
                System.out.println("比较次数" + i);
                if (a[i] > a[i + 1]) {
                    //实现两个数的交换
                    Utils.swap(a, i, i + 1);
                    swapped = true;
                }
            }
            System.out.println("第" + j + "轮冒泡"
                    + Arrays.toString(a));
            if (!swapped) {
                break;
            }
        }
    }
    public static void bubble_v2(int[] a) {
        int n = a.length - 1;
        while (true) {
            int last = 0; // 表示最后一次交换索引位置
            for (int i = 0; i < n; i++) {
                System.out.println("比较次数" + i);
                if (a[i] > a[i + 1]) {
                    Utils.swap(a, i, i + 1);
                    last = i;
                }
            }
            n = last;
            System.out.println("第轮冒泡"
                    + Arrays.toString(a));
            if (n == 0) {
                break;
            }
        }
    }
}
